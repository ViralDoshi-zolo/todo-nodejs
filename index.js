const mysql = require('mysql');
const express = require('express');
var app = express();
const bodyParser = require('body-parser');

app.use(bodyParser.json());

var mysqlConnection = mysql.createConnection({
  host: "127.0.0.1",
  user: "root",
  password: "password",
  database: "todonode"
});

mysqlConnection.connect(function(err) {
  if (err) throw err;
  console.log("Connected!");
});

app.listen(3000, ()=>console.log('Express server at port 3000'));

app.get('/users', function(req, res){

    mysqlConnection.query('SELECT * FROM user', (err, rows, fields)=>{
      if (!err)
        res.send(rows);
      else
        console.log(err);
    })
});

app.get('/users/:id', function(req, res){
  mysqlConnection.query('SELECT * FROM user WHERE user.user_id = ?', [req.params.id], (err, rows, fields)=>{
    if (!err)
      res.send(rows);
    else
      console.log(err);
  })
});

app.delete('/users/:id', function(req, res){
  mysqlConnection.query('DELETE FROM user WHERE user_id = ?', [req.params.id], (err, rows, fields)=>{
    if (!err)
      res.send("Deleted Successfully");
    else
      console.log(err);
  })
});

app.post('/users', function(req, res){

  var firstName = req.body.firstName.toString();
  var lastName = req.body.lastName.toString(); 
  mysqlConnection.query("INSERT INTO user (first_name, last_name) VALUES (?, ?)", [firstName, lastName], (err, rows, fields)=>{

  if (err)
    throw err;
  res.send("Success");
  });
});


app.put('/users/:id', function(req, res){

  var firstName = req.body.firstName.toString();
  var lastName = req.body.firstName.toString();
  mysqlConnection.query("UPDATE user SET first_name = ?, last_name = ? WHERE user_id = ?", [firstName, lastName, req.params.id] , (err, result)=>{
    if (err)
    throw err;
  res.send("Success");
  });
});